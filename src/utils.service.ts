import { HttpException, Injectable } from '@nestjs/common';

@Injectable()
export class UtilsService {
  public catchAuth0Error() {
    return (error) => {
      console.log(error);
      if (error.response) {
        throw new HttpException(
          error.response.data.error_description,
          error.response.status,
        );
      } else {
        throw new HttpException(error.message, 500);
      }
    };
  }
  public catchERPerror() {
    return (error) => {
      throw new HttpException(error.message, 500);
    };
  }
}