import { UtilsService } from './utils.service';
import { HttpException } from '@nestjs/common';

describe('UtilsService', () => {
  let utilsService: UtilsService;

  beforeEach(() => {
    utilsService = new UtilsService();
  });

  describe('catchAuth0Error', () => {
    it('should throw HttpException with error description and status from response if error.response exists', () => {
      // Arrange
      const error = {
        response: {
          data: {
            error_description: 'Invalid credentials',
          },
          status: 401,
        },
      };

      // Act and assert
      expect(() => utilsService.catchAuth0Error()(error)).toThrow(
        new HttpException('Invalid credentials', 401),
      );
    });

    it('should throw HttpException with error message and status 500 if error.response does not exist', () => {
      // Arrange
      const error = new Error('An error occurred');

      // Act and assert
      expect(() => utilsService.catchAuth0Error()(error)).toThrow(
        new HttpException('An error occurred', 500),
      );
    });
  });

  describe('catchERPerror', () => {
    it('should throw an HttpException with the correct message and status code', () => {
      const errorMessage = 'Test error message';
      const statusCode = 500;
      const catchErrorFn = utilsService.catchERPerror();

      try {
        catchErrorFn({ message: errorMessage });
      } catch (error) {
        expect(error).toBeInstanceOf(HttpException);
        expect(error.message).toEqual(errorMessage);
        expect(error.getStatus()).toEqual(statusCode);
      }
    });
  });
});