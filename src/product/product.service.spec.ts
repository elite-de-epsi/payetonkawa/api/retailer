import { Test, TestingModule } from '@nestjs/testing';
import { ProductService } from './product.service';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { HttpModule, HttpService } from '@nestjs/axios';
import { UtilsService } from '../utils.service';
import { lastValueFrom, of } from 'rxjs';
import { ProductMockData } from './product.mock';
import { HttpException, HttpStatus } from '@nestjs/common';
import { ProductModule } from './product.module';

describe('ProductsService', () => {
  let service: ProductService;
  let httpService: HttpService;
  let configService: ConfigService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [ConfigModule.forRoot(), ProductModule, HttpModule],
      providers: [
        ProductService,
        UtilsService,
        {
          provide: HttpService,
          useValue: {
            get: jest.fn(),
          },
        },
        {
          provide: ConfigService,
          useValue: {
            get: jest.fn(),
          },
        },
      ],
    }).compile();

    service = module.get<ProductService>(ProductService);
    httpService = module.get<HttpService>(HttpService);
    configService = module.get<ConfigService>(ConfigService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('getProducts', () => {
    it('test for getProducts', async () => {
      jest
        .spyOn(httpService, 'get')
        .mockImplementation(() => of({ data: ProductMockData.listERP } as any));
      jest.spyOn(configService, 'get').mockReturnValue('https://test.fr/');

      const result = await lastValueFrom(service.getProducts());

      expect(result).toEqual(ProductMockData.listProduct);
    });

    it('', async () => {
      expect.assertions(2);

      jest.spyOn(httpService, 'get').mockImplementation(() => {
        throw new HttpException('An error', HttpStatus.BAD_REQUEST);
      });
      jest.spyOn(configService, 'get').mockReturnValue('https://test.fr/');

      try {
        await lastValueFrom(service.getProducts());
      } catch (e) {
        expect(e).toBeInstanceOf(HttpException);
        expect(e.status).toBe(400);
      }
    });
  });

  describe('getProductById', () => {
    it('test for getProductById', async () => {
      jest
        .spyOn(httpService, 'get')
        .mockImplementation(() => of({ data: ProductMockData.ERP } as any));
      jest.spyOn(configService, 'get').mockReturnValue('https://test.fr/');

      const result = await lastValueFrom(
        service.getProductById(ProductMockData.productTestID),
      );

      expect(result).toEqual(ProductMockData.product);
    });

    it('', async () => {
      expect.assertions(2);

      jest.spyOn(httpService, 'get').mockImplementation(() => {
        throw new HttpException('An error', HttpStatus.BAD_REQUEST);
      });
      jest.spyOn(configService, 'get').mockReturnValue('https://test.fr/');

      try {
        await lastValueFrom(
          service.getProductById(ProductMockData.productTestID),
        );
      } catch (e) {
        expect(e).toBeInstanceOf(HttpException);
        expect(e.status).toBe(400);
      }
    });
  });
});