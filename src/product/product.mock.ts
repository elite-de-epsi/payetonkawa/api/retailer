const ProductTestID = '10';

export const ProductMockData = {
  productTestID: ProductTestID,
  listERP: [
    {
      createdAt: '2023-02-20T13:19:51.820Z',
      name: 'Bennie Koelpin',
      details: {
        price: '181.00',
        description:
          'The beautiful range of Apple Naturalé that has an exciting mix of natural ingredients. With the Goodness of 100% Natural Ingredients',
        color: 'blue',
      },
      stock: 17148,
      id: ProductTestID,
    },
    {
      createdAt: '2023-02-19T23:44:10.933Z',
      name: 'Gene Beatty',
      details: {
        price: '323.00',
        description:
          'The Nagasaki Lander is the trademarked name of several series of Nagasaki sport bikes, that started with the 1984 ABC800J',
        color: 'grey',
      },
      stock: 41411,
      id: '11',
    },
  ],
  listProduct: [
    { id: '10', name: 'Bennie Koelpin', price: '181.00', color: 'blue' },
    { id: '11', name: 'Gene Beatty', price: '323.00', color: 'grey' },
  ],
  ERP: {
    createdAt: '2023-02-20T13:19:51.820Z',
    name: 'Bennie Koelpin',
    details: {
      price: '181.00',
      description:
        'The beautiful range of Apple Naturalé that has an exciting mix of natural ingredients. With the Goodness of 100% Natural Ingredients',
      color: 'blue',
    },
    stock: 17148,
    id: ProductTestID,
  },

  product: {
    id: '10',
    description:
      'The beautiful range of Apple Naturalé that has an exciting mix of natural ingredients. With the Goodness of 100% Natural Ingredients',
    name: 'Bennie Koelpin',
    price: '181.00',
    color: 'blue',
    stock: 17148,
  },
};