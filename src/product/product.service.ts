import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { AxiosRequestConfig } from 'axios';
import { catchError, map, Observable } from 'rxjs';
import { UtilsService } from '../utils.service';
import { Product } from './product.dto';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class ProductService {
  constructor(
    private httpService: HttpService,
    private configService: ConfigService,
    private utilsService: UtilsService,
  ) {}

  private httpOptions: AxiosRequestConfig = {
    headers: { 'Content-Type': 'application/json' },
  };

  getProducts(): Observable<Product[]> {
    return this.httpService
      .get(`${this.configService.get('ERP_PATH')}products`, this.httpOptions)
      .pipe(
        map((response) => response.data),
        map((products) =>
          products.map((product) => ({
            id: product.id,
            name: product.name,
            price: product.details.price,
            color: product.details.color,
          })),
        ),
        catchError(this.utilsService.catchERPerror()),
      );
  }

  getProductById(id: string): Observable<Product> {
    return this.httpService
      .get(
        `${this.configService.get('ERP_PATH')}products/${id}`,
        this.httpOptions,
      )
      .pipe(
        map((response) => response.data),
        map((product) => ({
          id: product.id,
          name: product.name,
          price: product.details.price,
          description: product.details.description,
          color: product.details.color,
          stock: product.stock,
        })),
        catchError(this.utilsService.catchERPerror()),
      );
  }
}