import { Test, TestingModule } from '@nestjs/testing';
import { ProductController } from './product.controller';
import { ProductService } from './product.service';
import { ProductModule } from './product.module';
import { lastValueFrom, of } from 'rxjs';
import { ProductMockData } from './product.mock';

describe('ProductsController', () => {
  let controller: ProductController;
  let productService: ProductService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [ProductModule],
      controllers: [ProductController],
      providers: [
        {
          provide: ProductService,
          useValue: {
            getProducts: jest.fn(),
            getProductById: jest.fn(),
          },
        },
      ],
    }).compile();

    controller = module.get<ProductController>(ProductController);
    productService = module.get<ProductService>(ProductService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('getProducts', () => {
    it('', async () => {
      jest
        .spyOn(productService, 'getProducts')
        .mockImplementation(() => of(ProductMockData.listProduct as any));

      const result = await lastValueFrom(controller.getProducts());

      expect(result).toEqual(ProductMockData.listProduct);
    });
  });

  describe('getProductById', () => {
    it('', async () => {
      jest
        .spyOn(productService, 'getProductById')
        .mockImplementation(() => of(ProductMockData.product as any));

      const result = await lastValueFrom(
        controller.getProductById(ProductMockData.productTestID),
      );

      expect(result).toEqual(ProductMockData.product);
    });
  });
});