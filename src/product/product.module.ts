import { Module } from '@nestjs/common';
import { ProductController } from './product.controller';
import { ProductService } from './product.service';
import { HttpModule } from '@nestjs/axios';
import { ConfigService } from '@nestjs/config';
import { UtilsService } from '../utils.service';

@Module({
  imports: [HttpModule],
  controllers: [ProductController],
  providers: [ProductService, ConfigService, UtilsService],

  exports: [ProductService],
})
export class ProductModule {}