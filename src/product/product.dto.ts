export interface Product {
  id: string;
  name: string;
  price: number;
  description: string;
  color: string;
  stock: string;
}