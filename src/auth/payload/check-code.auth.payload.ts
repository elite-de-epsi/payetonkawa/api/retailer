import { PartialType } from '@nestjs/swagger';
import { SendEmailAuthPayload } from './send-email.auth.payload';

export class CheckCodeAuthPayload extends PartialType(SendEmailAuthPayload) {
  code: string;
}
