import { Test, TestingModule } from '@nestjs/testing';
import { AuthService } from './auth.service';
import { HttpModule, HttpService } from '@nestjs/axios';
import { lastValueFrom, of } from 'rxjs';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { AuthModule } from '../auth.module';
import { AuthMockData } from '../auth.mock';
import { HttpException, HttpStatus } from '@nestjs/common';
import { UtilsService } from '../../utils.service';

describe('AuthService', () => {
  let authService: AuthService;
  let httpService: HttpService;
  let configService: ConfigService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [ConfigModule.forRoot(), AuthModule, HttpModule],
      providers: [
        AuthService,
        UtilsService,
        {
          provide: HttpService,
          useValue: {
            post: jest.fn(),
          },
        },
        {
          provide: ConfigService,
          useValue: {
            get: jest.fn(),
          },
        },
      ],
    }).compile();

    authService = module.get<AuthService>(AuthService);
    httpService = module.get<HttpService>(HttpService);
    configService = module.get<ConfigService>(ConfigService);
  });

  it('should be defined', () => {
    expect(authService).toBeDefined();
  });

  describe('sendEmailVerificationCode', () => {
    it('should call the Auth0 API and send an email verification code', async () => {
      jest
        .spyOn(httpService, 'post')
        .mockImplementation(() => of({ data: {} } as any));
      jest.spyOn(configService, 'get').mockReturnValue('test_value');

      const result = await lastValueFrom(
        authService.sendEmailVerificationCode(AuthMockData.email),
      );

      expect(result).toEqual(HttpStatus.OK);
    });

    it('should throw an error if the Auth0 API call failed', async () => {
      expect.assertions(2);

      jest.spyOn(httpService, 'post').mockImplementation(() => {
        throw new HttpException('An error', HttpStatus.BAD_REQUEST);
      });
      jest.spyOn(configService, 'get').mockReturnValue('test_value');

      try {
        await lastValueFrom(
          authService.sendEmailVerificationCode(AuthMockData.email),
        );
      } catch (e) {
        expect(e).toBeInstanceOf(HttpException);
        expect(e.status).toBe(400);
      }
    });
  });

  describe('checkEmailVerificationCode', () => {
    it('should call the Auth0 API and check the email verification code', async () => {
      jest
        .spyOn(httpService, 'post')
        .mockImplementation(() =>
          of({ data: AuthMockData.authTokenDto } as any),
        );
      jest.spyOn(configService, 'get').mockReturnValue('test_value');

      const result = await lastValueFrom(
        authService.checkEmailVerificationCode(
          AuthMockData.email,
          AuthMockData.code,
        ),
      );

      expect(result).toEqual(AuthMockData.authTokenDto);
    });

    it('should throw an error if the Auth0 API call failed', async () => {
      expect.assertions(2);

      jest.spyOn(httpService, 'post').mockImplementation(() => {
        throw new HttpException('An error', HttpStatus.BAD_REQUEST);
      });
      jest.spyOn(configService, 'get').mockReturnValue('test_value');

      try {
        await lastValueFrom(
          authService.checkEmailVerificationCode(
            AuthMockData.email,
            AuthMockData.code,
          ),
        );
      } catch (e) {
        expect(e).toBeInstanceOf(HttpException);
        expect(e.status).toBe(400);
      }
    });
  });
});
