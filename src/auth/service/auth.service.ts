import { HttpStatus, Injectable } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { AxiosRequestConfig } from 'axios';
import { AuthTokenDto } from '../dto/auth.token.dto';
import { ConfigService } from '@nestjs/config';
import { UtilsService } from '../../utils.service';
import { catchError, map, Observable } from 'rxjs';

@Injectable()
export class AuthService {
  constructor(
    private httpService: HttpService,
    private configService: ConfigService,
    private utilsService: UtilsService,
  ) {}

  private httpOptions: AxiosRequestConfig = {
    headers: { 'Content-Type': 'application/json' },
  };

  sendEmailVerificationCode(email: string): Observable<number> {
    return this.httpService
      .post(
        `${this.configService.get(
          'AUTH0_AUTHENTICATION_API',
        )}passwordless/start`,
        {
          connection: 'email',
          email: email,
          send: 'code',
          client_id: this.configService.get('AUTH0_RETAILER_CLIENT_ID'),
          client_secret: this.configService.get('AUTH0_RETAILER_CLIENT_SECRET'),
        },
        this.httpOptions,
      )
      .pipe(
        map(() => HttpStatus.OK),
        catchError(this.utilsService.catchAuth0Error()),
      );
  }

  checkEmailVerificationCode(
    email: string,
    code: string,
  ): Observable<AuthTokenDto> {
    return this.httpService
      .post(
        `${this.configService.get('AUTH0_AUTHENTICATION_API')}oauth/token`,
        {
          grant_type: 'http://auth0.com/oauth/grant-type/passwordless/otp',
          username: email,
          otp: code,
          realm: 'email',
          client_id: this.configService.get('AUTH0_RETAILER_CLIENT_ID'),
          client_secret: this.configService.get('AUTH0_RETAILER_CLIENT_SECRET'),
          audience: this.configService.get('AUTH0_API_AUDIENCE'),
          scope: 'offline_access',
        },
        this.httpOptions,
      )
      .pipe(
        map((response) => response.data as AuthTokenDto),
        catchError(this.utilsService.catchAuth0Error()),
      );
  }
}
