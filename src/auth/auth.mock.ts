const email = 'test@example.com';
const code = '123456';

export const AuthMockData = {
  email: email,
  code: code,
  sendEmailAuthPayload: { email: email },
  checkCodeAuthPayload: { email: email, code: code },
  authTokenDto: {
    access_token: 'dummy_access_token',
    refresh_token: 'fake_refresh_token',
    expires_in: 0,
    token_type: 'Bearer',
    scope: 'read write',
  },
};
