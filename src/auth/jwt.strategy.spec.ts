import { Test } from '@nestjs/testing';
import { JwtStrategy } from './jwt.strategy';
import { ConfigService } from '@nestjs/config';

describe('JwtStrategy', () => {
  let jwtStrategy: JwtStrategy;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [
        JwtStrategy,
        {
          provide: ConfigService,
          useValue: {
            get: jest.fn().mockImplementation((key: string) => {
              switch (key) {
                case 'AUTH0_API_AUDIENCE':
                  return 'test-api-audience';
                case 'AUTH0_AUTHENTICATION_API':
                  return 'https://test-authentication-api/';
                default:
                  return null;
              }
            }),
          },
        },
      ],
    }).compile();

    jwtStrategy = moduleRef.get<JwtStrategy>(JwtStrategy);
  });

  it('should be defined', () => {
    expect(jwtStrategy).toBeDefined();
  });

  describe('validate', () => {
    it('should return the payload unchanged', () => {
      const payload = { sub: 'test-subject', username: 'test-username' };
      expect(jwtStrategy.validate(payload)).toEqual(payload);
    });
  });
});
