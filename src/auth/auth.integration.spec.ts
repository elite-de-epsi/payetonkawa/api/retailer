import { Test, TestingModule } from '@nestjs/testing';
import { AuthController } from './controller/auth.controller';
import { AuthService } from './service/auth.service';
import { AuthModule } from './auth.module';
import { HttpModule } from '@nestjs/axios';
import { ConfigModule } from '@nestjs/config';
import { AuthMockData } from './auth.mock';
import { lastValueFrom, of } from 'rxjs';
import { HttpStatus } from '@nestjs/common';
import { UtilsService } from '../utils.service';

describe('AuthController (Integration)', () => {
  let authController: AuthController;
  let authService: AuthService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [ConfigModule.forRoot(), AuthModule, HttpModule],
      controllers: [AuthController],
      providers: [AuthService, UtilsService],
    }).compile();

    authController = module.get<AuthController>(AuthController);
    authService = module.get<AuthService>(AuthService);
  });

  it('should be defined', () => {
    expect(authController).toBeDefined();
  });

  describe('sendEmailVerificationCode', () => {
    it('should call sendEmailVerificationCode and return HttpStatus.OK', async () => {
      const authServiceSpy = jest
        .spyOn(authService, 'sendEmailVerificationCode')
        .mockImplementation(() => of(HttpStatus.OK));

      await lastValueFrom(
        authController.sendEmailVerificationCode(
          AuthMockData.sendEmailAuthPayload,
        ),
      );

      expect(authServiceSpy).toHaveBeenCalledWith(AuthMockData.email);
    });
  });

  describe('checkCode', () => {
    it('should call checkEmailVerificationCode and return an AuthTokenDto', async () => {
      const authServiceSpy = jest
        .spyOn(authService, 'checkEmailVerificationCode')
        .mockImplementation(() => of(AuthMockData.authTokenDto));

      await lastValueFrom(
        authController.checkCode(AuthMockData.checkCodeAuthPayload),
      );

      expect(authServiceSpy).toHaveBeenCalledWith(
        AuthMockData.email,
        AuthMockData.code,
      );
    });
  });
});
