import { Controller, Post, Body } from '@nestjs/common';
import { AuthService } from '../service/auth.service';
import { SendEmailAuthPayload } from '../payload/send-email.auth.payload';
import { CheckCodeAuthPayload } from '../payload/check-code.auth.payload';
import { AuthTokenDto } from '../dto/auth.token.dto';
import { ApiTags } from '@nestjs/swagger';
import { Observable } from 'rxjs';

@Controller('auth')
@ApiTags('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  /**
   * Method which permit to send a sms verification code
   * Token is required.
   *
   * @param payload
   * @return {number} - Customers which correspond
   */
  @Post('send-email')
  sendEmailVerificationCode(
    @Body() payload: SendEmailAuthPayload,
  ): Observable<number> {
    return this.authService.sendEmailVerificationCode(payload.email);
  }

  /**
   * Method which permit to check the code sent by email.
   *
   * @param payload
   * @return {AuthTokenDto} - New token with new permissions.
   */
  @Post('check-code')
  checkCode(@Body() payload: CheckCodeAuthPayload): Observable<AuthTokenDto> {
    return this.authService.checkEmailVerificationCode(
      payload.email,
      payload.code,
    );
  }
}
